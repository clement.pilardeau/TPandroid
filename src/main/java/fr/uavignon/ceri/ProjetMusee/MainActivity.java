package fr.uavignon.ceri.ProjetMusee;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.snackbar.Snackbar;

import fr.uavignon.ceri.ProjetMusee.data.database.ItemDao;

public class MainActivity extends AppCompatActivity {

    ListViewModel listViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        listViewModel = new ViewModelProvider(this).get(ListViewModel.class);
        listViewModel.loadCollectionFromDataBaseByYounger();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_delete_everything) {

            Snackbar.make(getWindow().getDecorView().getRootView()
                    , "Suppression en cours",
                    Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();

            listViewModel.deleteEverything();

            return true;

        }

        if (id == R.id.action_update_all) {

            Snackbar.make(getWindow().getDecorView().getRootView()
                    , "Mise à jour en cours",
                    Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();

            listViewModel.loadCollection();

            return true;
        }

        if (id == R.id.action_sort_by_younger) {

            Snackbar.make(getWindow().getDecorView().getRootView()
                    , "Tri en cours",
                    Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();

            listViewModel.loadCollectionFromDataBaseByYounger();

            return true;
        }

        if (id == R.id.action_sort_by_older) {

            Snackbar.make(getWindow().getDecorView().getRootView()
                    , "Tri en cours",
                    Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();

            listViewModel.loadCollectionFromDataBaseByOlder();

            return true;
        }

        if (id == R.id.action_sort_by_name) {

            Snackbar.make(getWindow().getDecorView().getRootView()
                    , "Tri en cours",
                    Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();

            listViewModel.loadCollectionFromDataBaseByName();

            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}