package fr.uavignon.ceri.ProjetMusee;


import android.app.Activity;
import android.content.Context;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import fr.uavignon.ceri.ProjetMusee.data.Item;

public class RecyclerAdapter extends RecyclerView.Adapter<fr.uavignon.ceri.ProjetMusee.RecyclerAdapter.ViewHolder> {

    private static int compteur = 1;
    private ArrayList<Item> itemList;
    private ListViewModel listViewModel;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View v;

        if(compteur%2 == 0) {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.card_layout1, viewGroup, false);
        }
        else {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.card_layout2, viewGroup, false);
        }
        compteur++;
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        viewHolder.itemTitle.setText(itemList.get(i).getName());

        if(itemList.get(i).getImage() != null) {
            Glide.with(viewHolder.itemView.getContext())
                    .load(itemList.get(i).getImage())
                    .into(viewHolder.imgItem);
        }

        String brand = String.valueOf(itemList.get(i).getBrand());
        String Year = String.valueOf(itemList.get(i).getYear());
        if(Integer.parseInt(Year) <= 0) {
            Year = "Année inconnue";
        }
        brand += " , " + Year;

        viewHolder.itemBrand.setText(brand);
    }

    @Override
    public int getItemCount() {

        if(itemList != null) {
            return  itemList.size();
        }
        else {
            return 0;
        }
    }

    public void setItemsList(ArrayList<Item> items) {

        itemList = items;
        notifyDataSetChanged();
    }

    public void setListViewModel(ListViewModel viewModel) {
        listViewModel = viewModel;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView itemTitle;
        TextView itemBrand;
        ImageView imgItem;

        ActionMode actionMode;
        String idSelectedLongClick;

        ViewHolder(View itemView) {
            super(itemView);
            itemTitle = itemView.findViewById(R.id.item_title);
            itemBrand = itemView.findViewById(R.id.item_brand);
            imgItem = itemView.findViewById(R.id.item_image);

            ActionMode.Callback actionModeCallback = new ActionMode.Callback() {

                @Override
                public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                    return true;
                }

                @Override
                public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                    return false;
                }

                @Override
                public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                    return false;
                }

                @Override
                public void onDestroyActionMode(ActionMode mode) {
                    actionMode = null;
                }
            };

            itemView.setOnClickListener(v -> {

                String id = RecyclerAdapter.this.itemList.get(getAdapterPosition()).getId();
                ListFragmentDirections  .ActionListFragmentToDetailFragment action = ListFragmentDirections.actionListFragmentToDetailFragment(id);
                action.setItemNum(id);
                Navigation.findNavController(v).navigate(action);

            });

            itemView.setOnLongClickListener(v -> {

                idSelectedLongClick = RecyclerAdapter.this.itemList.get(getAdapterPosition()).getId();
                if (actionMode != null) {
                    return false;
                }

                Context context = v.getContext();
                actionMode = ((Activity)context).startActionMode(actionModeCallback);
                v.setSelected(true);

                return true;
            });
        }
     }
}