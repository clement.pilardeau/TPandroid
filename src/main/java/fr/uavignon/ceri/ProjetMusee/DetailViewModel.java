package fr.uavignon.ceri.ProjetMusee;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import fr.uavignon.ceri.ProjetMusee.data.ItemRepository;
import fr.uavignon.ceri.ProjetMusee.data.Item;

public class DetailViewModel extends AndroidViewModel {

    private MutableLiveData<Boolean> isLoading;
    private final ItemRepository repository;
    private MutableLiveData<Item> item;
    private final MutableLiveData<Throwable> webServiceThrowable;

    public DetailViewModel (Application application) {

        super(application);
        isLoading= new MutableLiveData<>();
        repository = ItemRepository.get(application);
        isLoading=repository.isLoading;
        webServiceThrowable=repository.webServiceThrowable;
        item = new MutableLiveData<>();
    }

    public void setItem(String id) {

        repository.getItem(id);
        item = repository.getSelectedItem();
    }

    LiveData<Item> getItem() {
        return this.item;
    }

    LiveData<Boolean> getIsLoading() {
        return this.isLoading;
    }

    LiveData<Throwable> getWebServiceThrowable(){return this.webServiceThrowable;}
}