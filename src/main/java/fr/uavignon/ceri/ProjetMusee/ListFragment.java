package fr.uavignon.ceri.ProjetMusee;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ListFragment extends Fragment {

    private ListViewModel viewModel;

    private RecyclerAdapter adapter;
    private ProgressBar progress;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(ListViewModel.class);

        listenerSetup();
        observerSetup();
    }

    private void listenerSetup() {

        RecyclerView recyclerView = requireView().findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new RecyclerAdapter();
        recyclerView.setAdapter(adapter);
        progress = getView().findViewById(R.id.progressList);
        adapter.setListViewModel(viewModel);
    }

    private void observerSetup() {

        viewModel.getAllItems().observe(getViewLifecycleOwner(),
                items -> adapter.setItemsList(items));

        viewModel.getIsLoading().observe(getViewLifecycleOwner(),
                isLoading ->{
                    if(isLoading){
                        progress.setVisibility(View.VISIBLE);
                    }
                    else{
                        progress.setVisibility(View.GONE);
                    }
                }
            );
    }
}