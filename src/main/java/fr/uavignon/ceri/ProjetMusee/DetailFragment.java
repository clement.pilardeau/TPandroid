package fr.uavignon.ceri.ProjetMusee;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;

import java.util.Objects;

public class DetailFragment extends Fragment {

    private DetailViewModel viewModel;
    private TextView textNomItem, textDescription, textFonctionnel, textAnnee, textMarque, textId ;
    private ProgressBar progressBar;
    private ImageView imgItem;


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        return inflater.inflate(R.layout.fragment_detail, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(DetailViewModel.class);

        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        String itemID = args.getItemNum();
        viewModel.setItem(itemID);

        listenerSetup();
        observerSetup();

    }


    private void listenerSetup() {
        textNomItem = getView().findViewById(R.id.itemName);
        textDescription = getView().findViewById(R.id.description);
        textFonctionnel = getView().findViewById(R.id.editFonctionnel);
        textAnnee = getView().findViewById(R.id.editAnnee);
        textMarque = getView().findViewById(R.id.editMarque);
        textId = getView().findViewById(R.id.editId);
        imgItem = getView().findViewById(R.id.itemThumbnail);

        progressBar = getView().findViewById(R.id.progress);

        getView().findViewById(R.id.buttonLike).setOnClickListener(view -> {});

        getView().findViewById(R.id.buttonBack).setOnClickListener(view -> NavHostFragment.findNavController(DetailFragment.this)
                .navigate(R.id.action_DetailFragment_to_ListFragment)
            );
    }

    private void observerSetup() {
        viewModel.getItem().observe(getViewLifecycleOwner(),
                item -> {
                    if (item != null) {

                        textNomItem.setText(item.getName());

                        if(item.getImage() != null) {
                            Glide.with(this)
                                    .load(item.getImage())
                                    .into(imgItem);
                        }

                        textDescription.setText(item.getDescription());

                        if(item.getWorking()) {
                            textFonctionnel.setText("Oui");
                        }
                        else {
                            textFonctionnel.setText("Non");
                        }

                        if(item.getYear() <= 0) {
                            textAnnee.setText("Année inconnue");
                        }
                        else {
                            textAnnee.setText(String.valueOf(item.getYear()));
                        }
                        textMarque.setText(item.getBrand());

                        textId.setText(item.getId());
                    }
                }
            );

    viewModel.getIsLoading().observe(getViewLifecycleOwner(),
            isLoading ->{
                if(isLoading){
                    progressBar.setVisibility(View.VISIBLE);
                }
                else{
                    progressBar.setVisibility(View.GONE);
                }
            }
        );
    viewModel.getWebServiceThrowable().observe(getViewLifecycleOwner(),
                throwable ->{
                    Snackbar snackbar = Snackbar
                            .make(requireView(), Objects.requireNonNull(throwable.getMessage()), Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
        );
    }
}