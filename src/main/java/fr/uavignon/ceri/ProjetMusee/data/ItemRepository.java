package fr.uavignon.ceri.ProjetMusee.data;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import fr.uavignon.ceri.ProjetMusee.data.database.ItemDao;
import fr.uavignon.ceri.ProjetMusee.data.database.ItemRoomDatabase;
import fr.uavignon.ceri.ProjetMusee.data.webservice.ItemResponse;
import fr.uavignon.ceri.ProjetMusee.data.webservice.ItemInterface;
import fr.uavignon.ceri.ProjetMusee.data.webservice.ItemResult;
import retrofit2.*;
import retrofit2.converter.moshi.MoshiConverterFactory;

import static fr.uavignon.ceri.ProjetMusee.data.database.ItemRoomDatabase.databaseWriteExecutor;

public class ItemRepository {

    private final MutableLiveData<Item> selectedItem = new MutableLiveData<>();

    private final ItemInterface api;

    public MutableLiveData<Boolean> isLoading = new MutableLiveData<>();
    public MutableLiveData<Throwable> webServiceThrowable = new MutableLiveData<>();

    private final ItemDao itemDao;

    volatile int nbAPILoads = 0;

    private final MutableLiveData<ArrayList<Item>> items = new MutableLiveData<>();

    public MutableLiveData<ArrayList<Item>> getItems() {
        return items;
    }

    private static volatile ItemRepository INSTANCE;

    public synchronized static ItemRepository get(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new ItemRepository(application);
        }

        return INSTANCE;
    }

    public ItemRepository(Application application) {
        ItemRoomDatabase db = ItemRoomDatabase.getDatabase(application);
        itemDao = db.itemDao();
        Retrofit retrofit=
                new Retrofit.Builder()
                        .baseUrl("https://demo-lia.univ-avignon.fr/cerimuseum/")
                        .addConverterFactory(MoshiConverterFactory.create())
                        .build();

        api = retrofit.create(ItemInterface.class);
    }

    public MutableLiveData<Item> getSelectedItem() {
        return selectedItem;
    }

    public void deleteEverything(){
        ArrayList<Item> allItems = (ArrayList<Item>) itemDao.deleteEverything();
        items.postValue(allItems);
    }

    public void loadCollectionFromDataBaseByYounger(){
        ArrayList<Item> allItems = (ArrayList<Item>) itemDao.getAllItemsByYounger();
        items.postValue(allItems);
    }

    public void loadCollectionFromDataBaseByOlder(){
        ArrayList<Item> allItems = (ArrayList<Item>) itemDao.getAllItemsByOlder();
        allItems.addAll(itemDao.getAllItemsByOlderRest());
        items.postValue(allItems);
    }

    public void loadCollectionFromDataBaseByName(){
        ArrayList<Item> allItems = (ArrayList<Item>) itemDao.getAllItemsByName();
        items.postValue(allItems);
    }

    public void loadCollection(){
        isLoading.postValue(Boolean.TRUE);

        api.getCollection().enqueue(
                new Callback<Map<String, ItemResponse>>() {
                    @Override
                    public void onResponse(@NonNull Call<Map<String, ItemResponse>> call,
                                           @NonNull Response<Map<String, ItemResponse>> response) {
                        ArrayList<Item> itemTMP = new ArrayList<>();
                        int i=0;
                        assert response.body() != null;
                        for (String key: response.body().keySet()) {
                            ItemResult.transferInfo(Objects.requireNonNull(response.body().get(key)), key, itemTMP);
                            insertItem(itemTMP.get(i));
                            i=i+1;
                        }

                        items.setValue(itemTMP);

                        isLoading.postValue(Boolean.FALSE);
                    }

                    @Override
                    public void onFailure(@NonNull Call<Map<String, ItemResponse>> call, @NonNull Throwable t) {
                        if (nbAPILoads<=1){
                            isLoading.postValue(Boolean.FALSE);
                        }
                        else{
                            nbAPILoads=nbAPILoads-1;
                        }
                        webServiceThrowable.postValue(t);
                    }
                });
    }

    public void insertItem(Item newItem) {
        Future<Long> flong = databaseWriteExecutor.submit(() -> itemDao.insert(newItem));
        try {
            flong.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void getItem(String id)  {

        Future<Item> fitem = databaseWriteExecutor.submit(() -> itemDao.getItemById(id));
        try {
            selectedItem.setValue(fitem.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}