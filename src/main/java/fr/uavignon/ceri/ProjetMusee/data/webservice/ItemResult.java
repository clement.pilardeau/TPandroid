package fr.uavignon.ceri.ProjetMusee.data.webservice;

import java.util.ArrayList;

import fr.uavignon.ceri.ProjetMusee.data.Item;

public class ItemResult {

    public static void transferInfo(ItemResponse body, String key,ArrayList<Item> items) {

        Item item = new Item();
        item.setId(key);
        item.setName(body.name);
        item.setDescription(body.description);
        item.setWorking(body.working);
        if (body.year!=null){
            item.setYear(body.year);
        }
        if (body.brand!=null){
            item.setBrand(body.brand);
        }
        item.setImage("https://demo-lia.univ-avignon.fr/cerimuseum/items/" + key + "/thumbnail");

        items.add(item);
    }
}
