package fr.uavignon.ceri.ProjetMusee.data;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName = "item_database", indices = {@Index(value = {"name", "description"},
        unique = true)})
public class Item {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name="id")
    private String id;

    @NonNull
    @ColumnInfo(name="name")
    private String name;

    @ColumnInfo(name="description")
    private String description;

    @ColumnInfo(name="brand")
    private String brand = "Inconnue";

    @NonNull
    @ColumnInfo(name="working")
    private Boolean working = false;

    @NonNull
    @ColumnInfo(name="year")
    private int year = -1;

    @ColumnInfo(name="image")
    private String image = null;

    public Item(@NonNull String id, @NonNull String name, String description) {
        this.id=id;
        this.name=name;
        this.description=description;
    }

    @Ignore
    public Item() {}

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    @NonNull
    public Boolean getWorking() {
        return working;
    }

    @NonNull
    public String getDescription() {
        return description;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setWorking(@NonNull Boolean working) {
        this.working = working;
    }

    public void setDescription(@NonNull String description) {
        this.description = description;
    }

    public void setName(@NonNull String name) {
         this.name = name;
    }

    public void setBrand(@NonNull String brand) {
        this.brand=brand;
    }

    @NonNull
    public String getBrand() {
        return brand;
    }

    public void setImage(@NonNull String img)
    {
        this.image = img;
    }

    public String getImage()
    {
        return this.image;

    }
}