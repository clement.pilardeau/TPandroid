package fr.uavignon.ceri.ProjetMusee.data.webservice;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface ItemInterface {
    @Headers({"Accept: json"})
    @GET("collection")
    Call<Map<String,ItemResponse>> getCollection();
}
