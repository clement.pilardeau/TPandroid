package fr.uavignon.ceri.ProjetMusee.data.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import fr.uavignon.ceri.ProjetMusee.data.Item;

@Dao
public interface ItemDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(Item item);

    @Query("SELECT * FROM item_database WHERE id = 'rien'")
    List<Item> deleteEverything();

    @Query("SELECT * FROM item_database WHERE id = :id")
    Item getItemById(String id);

    @Query("SELECT * from item_database ORDER BY year DESC")
    List<Item> getAllItemsByYounger();

    @Query("SELECT * from item_database WHERE year > 0 ORDER BY year ASC")
    List<Item> getAllItemsByOlder();

    @Query("SELECT * from item_database WHERE year <= 0")
    List<Item> getAllItemsByOlderRest();

    @Query("SELECT * from item_database ORDER BY name ASC")
    List<Item> getAllItemsByName();
}