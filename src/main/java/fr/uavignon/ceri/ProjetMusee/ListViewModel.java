package fr.uavignon.ceri.ProjetMusee;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.ArrayList;

import fr.uavignon.ceri.ProjetMusee.data.*;

public class ListViewModel extends AndroidViewModel {

    private final ItemRepository repository;
    private final MutableLiveData<Boolean> isLoading;

    private MutableLiveData<ArrayList<Item>> allItems;

    public ListViewModel (Application application) {

        super(application);
        allItems=new MutableLiveData<>();
        repository = ItemRepository.get(application);
        isLoading=repository.isLoading;
        allItems=repository.getItems();
    }

    LiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    LiveData<ArrayList<Item>> getAllItems() {
        return allItems;
    }

    public void loadCollection(){
        Thread t = new Thread(){
            public void run(){
                repository.loadCollection();

            }
        };
        t.start();
    }

    public void deleteEverything(){
        Thread t = new Thread(){
            public void run(){
                repository.deleteEverything();
            }
        };
        t.start();
    }

    public void loadCollectionFromDataBaseByYounger(){
        Thread t = new Thread(){
            public void run(){
                repository.loadCollectionFromDataBaseByYounger();

            }
        };
        t.start();
    }

    public void loadCollectionFromDataBaseByOlder(){
        Thread t = new Thread(){
            public void run(){
                repository.loadCollectionFromDataBaseByOlder();

            }
        };
        t.start();
    }

    public void loadCollectionFromDataBaseByName(){
        Thread t = new Thread(){
            public void run(){
                repository.loadCollectionFromDataBaseByName();

            }
        };
        t.start();
    }

}
